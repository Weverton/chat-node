# README #

Este README explica o que foi feito do desafio enviado

### Plataforma utilizada ###
Node.js (Express.js com Socket.io)

### Instalação ###
Entrar no diretório raiz, onde se encontra o arquivo package.json e executar o comando npm install

### O que foi implementado? ###
- Tela de login, chat e múltiplas salas.

### O que poderia ser implementado? ###
- Seleção de salas existentes o que seria carregado a partir da lista de sockets do framework
- Testes (Por não ter tanta experiência, tive dificuldades)
- Processo de deploy automatizado (Tentei correr atrás mas faltou tempo para implementar algo nesse sentido)
