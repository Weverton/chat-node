var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/view/index.html');
});

// Permite postar em uma sala existente
app.post('/chat/post_message', function(req, res) {

    io.to(req.body.room).emit('message', req.body.user + ' diz: ' + req.body.message);

});

io.on('connection', function(socket){

    var user;
    var room;

    socket.on('disconnect', function(){
        io.to(room).emit('message', user + ' saiu da sala');
    });

    socket.on('room', function(data) {
        var obj = JSON.parse(data);
        user = obj.user;
        room = obj.room;

        socket.join(room, function(){
            io.to(room).emit('title', room);
            io.to(room).emit('message', user + ' entrou na sala');
        });
    });

});

http.listen(3000, function(){
    console.log('listening on *:3000');
});
